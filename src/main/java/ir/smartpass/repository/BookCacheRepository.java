package ir.smartpass.repository;

import ir.smartpass.config.properties.GoogleBookProperties;
import ir.smartpass.dto.external.googleBook.response.BookResult;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Repository;

@Repository
public class BookCacheRepository {
    private static final String PREFIX = "Book_";
    private final ValueOperations<String, BookResult> valueOps;
    private final RedisTemplate<String, BookResult> redisTemplate;
    private final GoogleBookProperties googleBookProperties;

    public BookCacheRepository(
            @Qualifier("bookTemplate") RedisTemplate<String, BookResult> redisTemplate,
            GoogleBookProperties googleBookProperties
    ) {
        this.valueOps = redisTemplate.opsForValue();
        this.redisTemplate = redisTemplate;
        this.googleBookProperties = googleBookProperties;
    }

    public boolean save(String key, BookResult bookResult) {
        String fullKey = getFullKey(key);
        valueOps.set(fullKey, bookResult, googleBookProperties.getCacheTtl());
        return true;
    }

    public BookResult get(String key) {
        String fullKey = getFullKey(key);
        return valueOps.get(fullKey);
    }

    private String getFullKey(String key) {
        return PREFIX + key;
    }
}
