package ir.smartpass.services.client.google;

import lombok.Data;

@Data
public class BookResponseDto {
    private String title;
    private String authors;
}