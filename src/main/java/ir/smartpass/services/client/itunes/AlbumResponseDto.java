package ir.smartpass.services.client.itunes;

import lombok.Data;

@Data
public class AlbumResponseDto {
    private String title;
    private String artistName;
}