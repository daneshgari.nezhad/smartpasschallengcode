package ir.smartpass.services.impl;

import ir.smartpass.config.properties.GoogleBookProperties;
import ir.smartpass.config.properties.ItunesProperties;
import ir.smartpass.dto.external.SearchMediaResultDto;
import ir.smartpass.dto.external.googleBook.response.BookResult;
import ir.smartpass.dto.external.itunes.response.MusicResult;
import ir.smartpass.enums.MediaType;
import ir.smartpass.services.interfaces.IBookService;
import ir.smartpass.services.interfaces.IMediaService;
import ir.smartpass.services.interfaces.ITunesService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Slf4j
public class MediaServiceImpl implements IMediaService {
    private final IBookService bookService;
    private final ITunesService albumService;
    private final GoogleBookProperties googleBookProperties;
    private final ItunesProperties itunesProperties;


    public MediaServiceImpl(BookServiceImpl bookService,
                            TunesServiceImpl albumService, GoogleBookProperties googleBookProperties, ItunesProperties itunesProperties) {
        this.bookService = bookService;
        this.albumService = albumService;
        this.googleBookProperties = googleBookProperties;
        this.itunesProperties = itunesProperties;
    }


    public List<SearchMediaResultDto> searchMedia(String searchTerm) {
        long startTime = System.currentTimeMillis();

        log.info("service: MediaServiceImpl, method: searchMedia, call service media ...");

        ExecutorService executor = Executors.newFixedThreadPool(2);

        CompletableFuture<BookResult> bookFuture = CompletableFuture.supplyAsync(() -> {
            long bookStartTime = System.currentTimeMillis();
            BookResult books = bookService.search(searchTerm);
            long bookEndTime = System.currentTimeMillis();
            log.info("Book search took " + (bookEndTime - bookStartTime) + " ms");
            return books;
        }, executor);

        CompletableFuture<MusicResult> albumFuture = CompletableFuture.supplyAsync(() -> {
            long albumStartTime = System.currentTimeMillis();
            MusicResult albums = albumService.search(searchTerm);
            long albumEndTime = System.currentTimeMillis();
            log.info("Album search took " + (albumEndTime - albumStartTime) + " ms");
            return albums;
        }, executor);

        CompletableFuture<List<SearchMediaResultDto>> mergedResult = bookFuture.thenCombine(albumFuture, (books, albums) -> {
            long mergeStartTime = System.currentTimeMillis();
            List<SearchMediaResultDto> result = mergeAndSort(books, albums);
            long mergeEndTime = System.currentTimeMillis();
            log.info("Merging and sorting took " + (mergeEndTime - mergeStartTime) + " ms");
            return result;
        });

        List<SearchMediaResultDto> finalResult = mergedResult.join();

        executor.shutdown();

        long endTime = System.currentTimeMillis();
        log.info("Total execution time for searchMedia: " + (endTime - startTime) + " ms");

        return finalResult;
    }

    private List<SearchMediaResultDto> mergeAndSort(BookResult bookResult, MusicResult musicResult) {
        log.info("call mergeAndSort method ...");
        HashMap<String, Integer> size = calculatedRate(bookResult, musicResult);
        return Stream.concat(
                        bookResult.getItems().subList(0, size.get("bookSize")).stream()
                                .map(book -> {
                                    SearchMediaResultDto result = new SearchMediaResultDto();
                                    result.setTitle(book.getVolumeInfo().getTitle());
                                    result.setAuthors(book.getVolumeInfo().getAuthors());
                                    result.setMediaType(MediaType.BOOK);
                                    return result;
                                }),
                        musicResult.getResults().subList(0, size.get("albumSize")).stream()
                                .map(album -> {
                                    SearchMediaResultDto result = new SearchMediaResultDto();
                                    result.setTitle(album.getCollectionName());
                                    result.setAuthors(List.of(album.getArtistName()));
                                    result.setMediaType(MediaType.ALBUM);
                                    return result;
                                })
                ).sorted(Comparator.comparing(SearchMediaResultDto::getTitle)) // Sort by title
                .collect(Collectors.toList());
    }

    private HashMap<String, Integer> calculatedRate(BookResult bookResult, MusicResult musicResult) {
        Integer bookSize = googleBookProperties.getPerPage();
        Integer albumSize = itunesProperties.getPerPage();

        if (Integer.valueOf(bookResult.getItems().size()).compareTo(googleBookProperties.getPerPage()) < 0) {
            bookSize = bookResult.getItems().size();
        }
        if (Integer.valueOf(musicResult.getResults().size()).compareTo(itunesProperties.getPerPage()) < 0) {
            albumSize = musicResult.getResults().size();
        }

        HashMap<String, Integer> result = new HashMap<>();
        result.put("bookSize", bookSize);
        result.put("albumSize", albumSize);

        return result;
    }
}



