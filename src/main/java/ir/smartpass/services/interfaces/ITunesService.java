package ir.smartpass.services.interfaces;

import ir.smartpass.dto.external.itunes.response.MusicResult;

public interface ITunesService {
    MusicResult search(String searchTerm);
}
