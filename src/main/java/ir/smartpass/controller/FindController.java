package ir.smartpass.controller;

import io.micrometer.core.annotation.Timed;
import ir.smartpass.dto.external.SearchMediaResultDto;
import ir.smartpass.services.interfaces.IMediaService;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author MohammdReza Daneshgari
 * @version 1.0.0
 * @apiNote I use Java 17 /Spring/Redis/actuator(metric and health check)/...
 * This project is a test for SmartPass, I provide rest api for interacting with client
 * This api get a parameter as a searchTerm and process that, if client search before that,if is available
 * in the cache ,It will return that otherwise ,It hit the related api (google or itunes) at end save in cache for next related request.
 * you can configure total of search per concept, for example you can set 5 result for book and set 3 result for music.
 * pay attention default total search per webClient is 5 , you can change that from yml file.
 * if you want to run that, you should run redis before that.
 */

@Slf4j
@RestController
@RequestMapping("/api/v1/search")
@Validated
public class FindController {
    private final IMediaService mediaService;

    public FindController(IMediaService mediaService) {
        this.mediaService = mediaService;
    }

    @Timed(value = "search.timer", description = "Time taken to process my API endpoint")
    @GetMapping()
    public List<SearchMediaResultDto> search(@RequestParam @NonNull String searchTerm) {
      return mediaService.searchMedia(searchTerm);
    }
}
