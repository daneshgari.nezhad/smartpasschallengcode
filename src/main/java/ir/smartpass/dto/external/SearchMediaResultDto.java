package ir.smartpass.dto.external;

import ir.smartpass.enums.MediaType;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class SearchMediaResultDto {
    private String title;
    private List<String> authors;
    private MediaType mediaType;
}
