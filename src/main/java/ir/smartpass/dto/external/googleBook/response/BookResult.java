package ir.smartpass.dto.external.googleBook.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BookResult {
    private String kind;
    private Integer totalItems;
    private List<BookItem> items = new ArrayList<>();
}
