package ir.smartpass.config;

import ir.smartpass.dto.external.googleBook.response.BookResult;
import ir.smartpass.dto.external.itunes.response.MusicResult;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

@Configuration
public class RedisConfig {



    @Bean(name = "musicTemplate")
    public RedisTemplate<String, MusicResult> musicResultRedisTemplate(RedisConnectionFactory factory) {
        RedisTemplate<String, MusicResult> template = new RedisTemplate<>();
        template.setConnectionFactory(factory);
        // Set key serializer
        StringRedisSerializer keySerializer = new StringRedisSerializer();
        template.setKeySerializer(keySerializer);
        template.setHashKeySerializer(keySerializer);
        // Set value serializer
        Jackson2JsonRedisSerializer<MusicResult> valueSerializer = new Jackson2JsonRedisSerializer<>(MusicResult.class);
        template.setValueSerializer(valueSerializer);
        template.setHashValueSerializer(valueSerializer);
        template.afterPropertiesSet();
        return template;
    }

    @Bean(name = "bookTemplate")
    public RedisTemplate<String, BookResult> bookResultRedisTemplate( RedisConnectionFactory factory) {
        RedisTemplate<String, BookResult> template = new RedisTemplate<>();
        template.setConnectionFactory(factory);
        // Set key serializer
        StringRedisSerializer keySerializer = new StringRedisSerializer();
        template.setKeySerializer(keySerializer);
        template.setHashKeySerializer(keySerializer);
        // Set value serializer
        Jackson2JsonRedisSerializer<BookResult> valueSerializer = new Jackson2JsonRedisSerializer<>(BookResult.class);
        template.setValueSerializer(valueSerializer);
        template.setHashValueSerializer(valueSerializer);

        template.afterPropertiesSet();
        return template;
    }
}
